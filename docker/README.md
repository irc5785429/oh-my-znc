# Max out some settings on the host (optional but helpful)
```
echo "net.ipv4.netfilter.ip_conntrack_max=196608" >> /etc/sysctl.conf
echo "* hard nofile 1024000" >> /etc/security/limits.conf
```

# Build a portage snapshot
```
docker create -v /usr/portage --name portage gentoo/portage:latest /bin/true
```

# Build the image
```
docker build -t netcrave/oh-my-znc -t netcrave/oh-my-znc:latest . 
```

# Set permissions locally
```
chown -R $(whoami):docker ~/oh-my-znc
```

# Run a new instance of the image
```
docker run -i -t -v /home/$(whoami)/oh-my-znc/:/home/znc/.znc/ -v /home/$(whoami)/oh-my-irssi/irssi:/home/znc/.irssi/ --cap-add net_bind_service --name irc --volumes-from portage netcrave/oh-my-znc:latest
```

# https://stackoverflow.com/questions/25744278/how-to-make-docker-run-inherit-ulimits
