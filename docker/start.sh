#!/bin/bash

emerge znc irssi su-exec || exit 1

setcap 'cap_net_bind_service=+ep' $(which znc)
chown -R znc:znc /home/znc

export HOME=/home/znc

cd ~/.znc/modules

export CXXFLAGS="-std=gnu++11"
znc-buildmod identserver.cpp

cd ~

chmod -R znc:znc /home/znc

su-exec znc:znc $(which znc) --datadir ~/.znc
su-exec znc:znc $(which irssi) --config ~/.irssi/config
